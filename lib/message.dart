import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  Message({this.text, this.name, this.type});

  final String text;
  final String name;
  final bool type;

  List<Widget> botMessage(context) {
    return <Widget>[
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  child: Text(this.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10)),
            ),
            Card(
              color: Colors.red[600],
              elevation: 3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Flexible(
                    child: Text(text, maxLines: 6, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),),)
                )
            ),
          ],
        ),
      ),
    ];
  }

  List<Widget> userMessage(context) {
    return <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Text(this.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10)),
            ),
            Card(
              color: Colors.orange[900],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(text, style: TextStyle(color: Colors.white, fontSize: 15),),
                )
            ),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(left: 10.0),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: this.type ? userMessage(context) : botMessage(context),
      ),
    );
  }
}